---
title: "2023 Khronos' Monado Improvements Project"
layout: main
---

- TOC
{:toc}

# Khronos' Monado Improvements Project 2023

This project ran from September 2023 to January 2024, with the purpose of
implementing specific features within the Monado OpenXR Runtime deemed useful
by the Khronos OpenXR Working Group.

Total hours: 480 hours

## RFP

The Khronos Group issued an [RFP][] for the Monado Improvements project on July
3rd 2023. Collabora was selected as the contractor for the project, which ran
from September 2023 to February 2024.

[RFP]: https://www.khronos.org/news/permalink/the-khronos-group-has-issued-a-rfp-for-monado-improvements

## Work done during this project

### Refactoring the layer "squashing" code to process each view separately - #2

**Closed**

Rework of Monado's compute renderer to dispatch the rendering process once per
view. This reduces the number of samplers required and allows a per-view target

- [Monado MR !1955](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/1955)
- [Monado MR !1957](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/1957)
- [Monado MR !1967](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/1967)

### Refactoring of the compositor graphics pipeline code to the new render architecture - #3

**Closed**

Complete rework of Monado's graphics pipeline, improving our Vulkan code on
all sides.

- [Monado MR !1969](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/1969)
- [Monado MR !1970](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/1970)
- [Monado MR !1971](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/1971)
- [Monado MR !1972](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/1972)
- [Monado MR !1974](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/1974)
- [Monado MR !1976](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/1976)
- [Monado MR !1983](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/1983)
- [Monado MR !1995](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/1995)
- [Monado MR !2026](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2026)
- [Monado MR !2101](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2101)
- [Monado MR !2103](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2103)
- [Monado MR !2105](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2105)

### New code for cylinder and equirect2 layer types to compute path and equirect - #4

**Closed**

Follow up of #2, the cylinder and equirect2 layers were deactivated during
the refactoring, the shaders were improved before plugging them back in.

- [Monado MR !1994](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/1994)
- [Monado MR !1998](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/1998)
- [Monado MR !2001](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2001)

### Addition of the layer "squasher" helper function(s) - #5

**Closed**

Refactor of the compositor's utils functions, to have better and more coherent
helper functions to take account of the device pose and scratch images.

- [Monado MR !1975](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/1975)
- [Monado MR !1981](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/1981)

### Implement `XR_KHR_visibility_mask` - #7

**Closed**

- [Monado MR !2016](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2016)
- [Monado MR !2032](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2032)
- [Monado MR !2034](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2034)

### Implement `XR_KHR_vulkan_swapchain_format_list` - #8

**Closed**

- [Monado MR !2049](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2049)
- [Monado MR !2052](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2052)
- [Monado MR !2083](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2083)

### Improve spaces - #9

**Closed**

Enhance the Monado "spaces" by implementing XR_EXT_local_floor and
XR_MSFT_unbounded_reference_space, and adding the capability to recenter.

- [Monado MR !2018](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2018)
- [Monado MR !2047](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2047)
- [Monado MR !2048](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2048)
- [Monado MR !2055](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2055)
- [Monado MR !2091](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2091)

### Implement `XrEventDataReferenceSpaceChangePending` - #10

**Closed**

- [Monado MR !2062](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2062)
- [Monado MR !2081](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2081)

### IPC improvement - #11

**Closed**

Improve the general state of the Monado IPC, make it more flexible and generic.

- [Monado MR !2009](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2009)
- [Monado MR !2011](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2011)
- [Monado MR !2012](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2012)
- [Monado MR !2015](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2015)
- [Monado MR !2022](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2022)
- [Monado MR !2025](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2025)
- [Monado MR !2028](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2028)
- [Monado MR !2030](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2030)
- [Monado MR !2046](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2046)
- [Monado MR !2053](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2053)
- [Monado MR !2079](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2079)
- [Monado MR !2095](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2095)
- [Monado MR !2096](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2096)

### Implementation of an interface for tracking plugins - #12

**Closed**

Add a C interface to Monado (with a C++ helper) to dynamically load VIT
(Visual Intertial Tracking) systems.

The VIT interface has been implemented in [Basalt][] as a testing ground
(not part of the Khronos-funded project).

- [Monado MR !2058](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2058)
- [Monado MR !2125](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2125)

We moved the interface files into their own repo.

- <https://gitlab.freedesktop.org/monado/utilities/vit>

### Implement `XR_EXT_conformance_automation` - #13

**In Progress**

- [Monado MR !2119](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/2119)

## Pending topics

These were tasks that were in scope for the project but could not be completed
in the funded time available.

### Enhancements to the compositor so that it uses `xrt_space` for poses

Follow up work for the Monado space work.

### Implementation of "Direct Mode" compositor target on Windows

Requires specialized hardware following the EDID extension for head-mounted
and specialized monitors [2] spec.

Some work has been on the subject 2 years ago.`
[Monado MR !738](https://gitlab.freedesktop.org/monado/monado/-/merge_requests/738)

### Implementation of Helper code and configuration format for device attachment hierarchy

Improve the xrt_device creation process to logically group devices together
without having to create a builder for every device combination.

### Work on extension : `XR_EXT_debug_utils`

All of the ground work is already done. The oxr_logger object in the
state tracker needs to be extended to collect and push messages to the
application. Optionally the work done on the IPC can be used to get data

### Work on extension : `XR_EXT_eye_gaze_interaction`

Parts of the work is already done in Monado, the state tracker part is done
and some work in the service is to be done.

Needs hardware supporting eyes tracking.

### Work on extension : `XR_EXT_hand_joints_motion_range`

A new interface in Monado to have hand-trackers per hand is needed.

### Work on extension : `XR_EXT_palm_pose`

The extension implementation is complete. The remaining work is to collect grip
pose to palm pose offset data from as many controllers as possible.

### Work on extension : `XR_EXT_win32_appcontainer_compatible`

Requires more Windows knowledge, figure out the installer and a way to create
the swapchain on the server and share the handles to the client due to
permission restrictions.

### Improvements to binding code to lay the foundations for user-driven input remapping, aka action based remapping

Requires a lot of rework on Monado's input code, blocked by another project.

### Mac support (Apple SoC)

This issue is almost a project of its own. We outlined the work in an
issue on the Monado repo.

[Monado issue #318](https://gitlab.freedesktop.org/monado/monado/-/issues/318)

[Basalt]: https://gitlab.freedesktop.org/mateosss/basalt
[2]: https://learn.microsoft.com/en-us/windows-hardware/drivers/display/specialized-monitors-edid-extension
